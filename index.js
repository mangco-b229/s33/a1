
// FETCH Array of todos (TITLE ONLY)
fetch("https://jsonplaceholder.typicode.com/todos")
	.then((res) => res.json())
	.then((json) => {
		let titles = json.map((item) => item.title)
		console.log(titles)
	})

// FETCH a single item of todo
// PRINT a message that provides the title and status of the item
let id = 1
fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)
  .then((res) => res.json())
  .then((json) => {
    let title = json.title
    let status = json.completed
    console.log(`The item "${title}", on the list has a status of ${status}`)
  })


// POST Method
let newTodo = {
	title: 'Created To Do List Item',
	completed: false
}

fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  body: JSON.stringify(newTodo),
  headers: {
    'Content-type': 'application/json'
  }
})
.then((response) => response.json())
.then((json) => console.log(json))

// PUT Method
// Update todo list Item by changing the data structure
let updatedTodo = {
    id: 1,
    title: 'Updated To Do List Item',
    description: 'Learn about the fetch API and how to make HTTP requests',
    status: 'Pending',
    dateCompleted: 'Pending',
    userId: 1
};

fetch(`https://jsonplaceholder.typicode.com/todos/${updatedTodo.id}`, {
  method: 'PUT',
  body: JSON.stringify(updatedTodo),
  headers: {
    'Content-type': 'application/json'
  }
})
.then((response) => response.json())
.then((json) => console.log(json))

// PATCH Method
let updatedTodoPatch = {
    status: 'Completed',
	dateCompleted: '07/09/21'    
};

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
  method: 'PATCH',
  body: JSON.stringify(updatedTodoPatch),
  headers: {
    'Content-type': 'application/json'
  }
})
.then((response) => response.json())
.then((json) => console.log(json))

// DELETE Method
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
  method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))







